FROM debian:latest

RUN mkdir -p /app
WORKDIR /app

ADD consignment.json /app/consignment.json
ADD consignment-manager-service /app/consignment-manager-service

CMD ["./consignment-manager-service"]