build:
	GOOS=linux GOARCH=amd64 go build
	docker build -t consignment-manager-service .

run:
	docker run consignment-manager-service