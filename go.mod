module bitbucket.org/tombrookson/consignment-manager-service

go 1.13

require (
	bitbucket.org/tombrookson/consignment-service v0.0.0-20191128210244-31521d26f472
	github.com/micro/go-micro v1.17.1
)
